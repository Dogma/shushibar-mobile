import { createStackNavigator, createAppContainer } from 'react-navigation';

import Main from './pages/main';

const MainNavigator = createStackNavigator({
    Main
}, {
    defaultNavigationOptions: {
        title: "SushiMobile",
        headerStyle: {
            backgroundColor: "#DA552F"
        },
        headerTintColor: "#FFF"
    }
});

const App = createAppContainer(MainNavigator);

export default App;