import React, { Component } from 'react';
import api from '../services/api';

import { View, Text } from 'react-native';

export default class Main extends Component {

    state = {
        cliente: [],
    };

    componentDidMount() {
        this.loadcliente();
    };

    loadcliente = async (page = 1) => {
        const response = await api.get(`/cliente`);
        const {data} = response;
        this.setState({cliente: data});
    };

    render() {
        const { cliente } = this.state;
        return (
            <View>
                {cliente.map(cliente => (
                    <View key={cliente.id}>
                        <Text>Nome: {cliente.nome}</Text>
                        <Text>Apelido: {cliente.apelido}</Text>
                        <Text>email: {cliente.email}</Text>
                    </View>
                ))}
            </View>            
        )
    };
}